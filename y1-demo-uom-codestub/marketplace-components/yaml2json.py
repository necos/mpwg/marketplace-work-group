#!/usr/bin/env python

import yaml
import json
import sys

def extract_json_values (json_input, keys):
    result={}
    for k in keys:
        subkeys=k.split('.')
        count=0
        for subkey in subkeys:
            if subkey.isdigit():
                subkeys[count]=int(subkeys[count])
            count=count+1

        if len(subkeys)==1:
            result[keys[k]]=json_input[subkeys[0]]
        if len(subkeys)==2:
            result[keys[k]]=json_input[subkeys[0]][subkeys[1]]
        if len(subkeys)==3:
            result[keys[k]]=json_input[subkeys[0]][subkeys[1]][subkeys[2]]
        if len(subkeys)==4:
            result[keys[k]]=json_input[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]]
        if len(subkeys)==5:
            result[keys[k]]=json_input[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]][subkeys[4]]
        if len(subkeys)==6:
            result[keys[k]]=json_input[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]][subkeys[4]][subkeys[5]]
        if len(subkeys)==7:
            result[keys[k]]=json_input[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]][subkeys[4]][subkeys[5]][subkeys[6]]
    return result

def set_json_values (json_input, attributes):
    json_output = json_input
    for k in attributes:
        subkeys=k.split('.')

        count=0
        for subkey in subkeys:
            if subkey.isdigit():
                subkeys[count]=int(subkeys[count])
            count=count+1

        if len(subkeys)==1:
            json_output[subkeys[0]] = attributes[k]
        if len(subkeys)==2:
            json_output[subkeys[0]][subkeys[1]] = attributes[k]
        if len(subkeys)==3:
            json_output[subkeys[0]][subkeys[1]][subkeys[2]] = attributes[k]
        if len(subkeys)==4:
            json_output[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]] = attributes[k]
        if len(subkeys)==5:
            json_output[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]][subkeys[4]] = attributes[k]
        if len(subkeys)==6:
            json_output[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]][subkeys[4]][subkeys[5]] = attributes[k]
        if len(subkeys)==7:
            json_output[subkeys[0]][subkeys[1]][subkeys[2]][subkeys[3]][subkeys[4]][subkeys[5]][subkeys[6]] = attributes[k]
    return json_output

def yaml_to_json(yaml_in):
    print("loading file: "+yaml_in)
    try:
        yaml_out = yaml.load(open(yaml_in), yaml.SafeLoader)
        print ("yaml loaded ok")
        try:
            json_str = json.dumps(yaml_out, indent =2)
            json_str = json_str.replace ('undefined','')

            print ("json created ok\n")
        except ValueError as e:
            print("json problem: %",e)
            return False

	#---return a json if all goes ok --------------------------------------
        return json_str

    except yaml.YAMLError as exc:
        print ("Error while parsing YAML file:")
        if hasattr(exc, 'problem_mark'):
            if exc.context != None:
				print ('  parser says\n' + str(exc.problem_mark) + '\n  ' + str(exc.problem) + ' ' + str(exc.context) + '\nPlease correct data and retry.\n')
            else:
                print ('  parser says\n' + str(exc.problem_mark) + '\n  ' + str(exc.problem) + '\nPlease correct data and retry.\n')
        else:
            print ("Something went wrong while parsing yaml file")
        return
#**** end of yaml_to_json **************************************************
