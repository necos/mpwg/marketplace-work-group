# !/usr/bin/env python
import json
import socket
from yaml2json import *
import os
from pygments import highlight, lexers, formatters
from config import *


# imports for ssh
import base64
import paramiko


# general WAN Slice Controller parameters
remote_testbeds=[];
local_testbed={};
ContentConfiguration=[];

deploy_routers = False
slice_duration = 120

#general parameters for configure unikernels
InitialCounterNumber=2

# slice output parameters
hostIPs={}
privateHostIPs={}
privateRouterIPs={}
routerIPs={}


# global variables for socket communication
host = '127.0.0.1'
port = 10083
Clients = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
Clients.connect((host, port))
response=[]
source="remoteWAN"

# DC Slice Controller input
json_object = None


def check_if_value_is_defined (value):
    if value=='':
        return False
    else:
        return True

def create_json_output (slice_output):
    count =0
    json_output = None
    json_prefix = ''
    for t in slice_output:
        for dc_slice_part in json_object['net-slice-part']['dc-slice']:
            if net_slice_part['dc-slice-part']['dc-slice-controller']['dc-slice-provider']==t['Provider']:
                json_prefix='dc-slice.'+str(count)+'.dc-slice-part.'
                json_output = set_json_values (json_object, {json_prefix+'physical-hosts.host-ips':t['NodeIPs'], json_prefix+'external-interface.host-ip':t['RouterIP'], json_prefix+'external-interface.host-private-ip':t['PrivateRouterIP'], json_prefix+'physical-hosts.host-private-ips':t['PrivateNodeIPs']})
            count=count+1

    #print (json.dumps(json_output, sort_keys=True, indent=4))

    return json_output


def create_wan_controller_input (json_object):
    #print json_object
    sp=[];
    sp2=[];

    # create input in the following form

    # iterate through WAN Slice Parts
    for i,wan_slice_part in enumerate(json_object['slices']['slice']):
        sp.append(extract_json_values(wan_slice_part,{'dc-slice-part.dc-slice-point-of-presence.name':'name','dc-slice-part.dc-slice-point-of-presence.host-ip':'RouterIP','dc-slice-part.dc-slice-point-of-presence.private-host-ip':'PrivateRouterIP','dc-slice-part.dc-slice-controller.dc-slice-provider':'Provider'}))


        for j,vdus in enumerate(wan_slice_part['dc-slice-part']['vdus']):

            sp2.append(extract_json_values(vdus,{'dc-vdu.id':'id','dc-vdu.epa-attributes.host-epa.host-ips':'NodeIPs','dc-vdu.epa-attributes.host-epa.host-private-ips':'PrivateNodeIPs','dc-vdu.epa-attributes.host-epa.host-names':'host-names','dc-vdu.epa-attributes.host-epa.subnet':'LocalPhysicalNetworkIPPrefix','dc-vdu.virtual-nodes-subnet':'VirtualNetworkIPPrefix','dc-vdu.epa-attributes.host-epa.host-count':'NodesNumber','dc-vdu.technology':'TypeOfVirtualTechology','dc-vdu.allocate-vdus':'ConfigureHypervisor','dc-vdu.virtual-nodes-count':'InitialDeployments'}))

            if (sp[i]['Provider'] == 'swn'):
                local_testbed.update({'Name':sp[i]['Provider'], 'RouterIP':sp[i]['RouterIP'], 'PrivateRouterIP':sp[i]['PrivateRouterIP'], 'NodeIPs':sp2[i]['NodeIPs'], 'PrivateNodeIPs':sp2[i]['PrivateNodeIPs'], 'UserName':local_testbed_username,'LocalPhysicalNetworkIPPrefix': sp2[i]['LocalPhysicalNetworkIPPrefix'], 'ConfigureHypervisor':sp2[i]['ConfigureHypervisor']});

                print ("current_local_testbed is: ")
                print (local_testbed)


            else:
                # deploy federated testbed
                current_testbed={'Name':sp[i]['Provider'],'PrivateRouterIP':sp[i]['PrivateRouterIP'],'RouterIP':sp[i]['RouterIP'], 'NodeIPs':sp2[i]['NodeIPs'], 'PrivateNodeIPs':sp2[i]['PrivateNodeIPs'], 'UserName':username,'LocalPhysicalNetworkIPPrefix': sp2[i]['LocalPhysicalNetworkIPPrefix'], 'ConfigureHypervisor':sp2[i]['ConfigureHypervisor'],'NodesNumber':sp2[i]['NodesNumber'],'TypeOfVirtualTechology':sp2[i]['TypeOfVirtualTechology'],'HostNameMachines':sp2[i]['host-names'],'InitialDeployments':sp2[i]['InitialDeployments'],'VirtualNetworkIPPrefix':sp2[i]['VirtualNetworkIPPrefix']};

                remote_testbeds.append(current_testbed)

                print ("remote_testbeds is: ")
                print (remote_testbeds)


# Main wan Slice Controller code

# Define socket daemon
while True:
    try:
        fragment_response=Clients.recv(1)
        if fragment_response != '\n':
            response.append(fragment_response)
        if fragment_response == '\n':
            json_msg=("".join(response))
            response=[]
            json_object_check=json.loads(json_msg)
            #print json_object
            if ("source" in json_object_check and "target" in json_object_check):
                if (json_object_check["source"] == "nodeWAN" and json_object_check["target"]== "remoteWAN"):
                    json_object=json_object_check["config"]
                    tcpLOG ={"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Wan-Slice-Controller","message":"Received message from the Slice Builder.","type":"communication","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    # create json object
                    #print ("the main json_object")
                    #print json_object
                    create_wan_controller_input(json_object)
                                        
                    #configuring ansible
                    print ("configure ansible for remoteUser")
                    #configuring Gre tunnels
                    secondIP="195.251.209.210"
                    firstIP="155.98.36.75"
                    firstusername="remoteUser"
                    secondusername="localUser"
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Wan-Slice-Controller","message":"Deploying GRE tunnel between "+secondIP+" and "+firstIP+" at host: "+firstIP+" with username "+firstusername,"type":"configuration","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    #configuration_ansible(remote_testbeds,username)
                    print ("Creating tunnel between 155.98.36.75 and 195.251.209.210")

                    # Executing tunneling commands from the main endpoint to all other testbed endpoints

                    print ("Execute add_gre_endpoint 195.251.209.210 155.98.36.75 uomgre0 10.2.0.250 10.1.0.0/24 at host 155.98.36.75 and username remoteUser")                    
                    

                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Wan-Slice-Controller","message":"Deploying GRE tunnel between "+firstIP+" and "+secondIP+" at host: "+secondIP+" with username "+secondusername,"type":"configuration","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    print ("Execute add_gre_endpoint 155.98.36.75 195.251.209.210 netUom0 10.1.0.250 10.2.0.0/24 at host 195.251.209.210 and username localUser")

                    # Fix all routing tables for GRE Tunneling
                    # First in remote testbeds, from all subnets to all
                    print("Fix local routing table at host 155.98.36.68 for subnet 10.1.0.0/24 and local router 10.2.0.250")
                    print("Fix local routing table at host 155.98.36.65 for subnet 10.1.0.0/24 and local router 10.2.0.250")
                    print("Fix local routing table at host 155.98.36.77 for subnet 10.1.0.0/24 and local router 10.2.0.250")
                    print("Fix local routing table at host 155.98.36.67 for subnet 10.1.0.0/24 and local router 10.2.0.250")
                    print("Fix local routing table at host 155.98.36.71 for subnet 10.1.0.0/24 and local router 10.2.0.250")
                    # Secondly in local testbed, no need it for now


                    #Configuring hypervisors for remote testbeds
                    print("Configuring hypervisor for node 155.98.36.68.")
                    print("Create virtual network at host 155.98.36.68, in username remoteUser for virtual-subnet 10.3 at 10.2.0.2")
                    print("Configuring hypervisor for node 155.98.36.65.")
                    print("Create virtual network at host 155.98.36.65, in username remoteUser for virtual-subnet 10.3 at 10.2.0.3")
                    print("Configuring hypervisor for node 155.98.36.77.")
                    print("Create virtual network at host 155.98.36.77, in username remoteUser for virtual-subnet 10.3 at 10.2.0.4")
                    print("Configuring hypervisor for node 155.98.36.67.")
                    print("Create virtual network at host 155.98.36.67, in username remoteUser for virtual-subnet 10.3 at 10.2.0.5")
                    print("Configuring hypervisor for node 155.98.36.71.")
                    print("Create virtual network at host 155.98.36.71, in username remoteUser for virtual-subnet 10.3 at 10.2.0.6")
                    
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Wan-Slice-Controller","message":"Sending response to the Slice Builder","type":"communication","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n") 
                    
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Resource Orchestrator","message":"Received message from the Slice Builder","type":"communication","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    #Configure Monitoring
                    print("Configure Monitoring for node with IP 155.98.36.68")
                    print("Configure Monitoring for node with IP 155.98.36.65")
                    print("Configure Monitoring for node with IP 155.98.36.77")
                    print("Configure Monitoring for node with IP 155.98.36.67")
                    print("Configure Monitoring for node with IP 155.98.36.71")
                    
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Resource Orchestrator","message":"Deploying monitoring, through IMA, to physical machines with IPs: 155.98.36.68, 155.98.36.65, 155.98.36.77, 155.98.36.67, 155.98.36.71","type":"configuration","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                                          


                    # configure and boot the VMs(Unikernel)
                    #example of configuration of VMs
                    ContentConfiguration=[{'UserName': 'remoteUser', 'Name': u'pg-utah', 'NodeIPs': [u'155.98.36.68', u'155.98.36.65', u'155.98.36.77', u'155.98.36.67', u'155.98.36.71'], 'NodesNumber': 5, 'ConfigureHypervisor': True, 'LocalPhysicalNetworkIPPrefix': u'10.2.0', 'PrivateRouterIP': u'10.2.0.1', 'VirtualNetworkIPPrefix': u'10.3', 'PrivateNodeIPs': [u'10.2.0.2', u'10.2.0.3', u'10.2.0.4', u'10.2.0.5', u'10.2.0.6'], 'NumberOfTunnels': 1, 'RouterIP': u'155.98.36.75', 'ContentConfiguration': [{'InitialDeployments': 5, 'PreFixUnikernelIPs': [u'10.3.2.2', u'10.3.3.2', u'10.3.4.2', u'10.3.5.2', u'10.3.6.2'], 'TypeOfVirtualTechology': u'Click-os', 'content_url': 'media0.swn.uom.gr'}], 'TypeOfVirtualTechology': u'Click-os', 'HostNameMachines': [u'pc768.emulab.net', u'pc765.emulab.net', u'pc777.emulab.net', u'pc767.emulab.net', u'pc771.emulab.net'], 'InitialDeployments': 5}]
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Resource Orchestrator","message":"Deploying virtual resources through IMA, details: 10.3.2.2, 10.3.3.2, 10.3.4.2, 10.3.5.2, 10.3.6.2 VMs with Unikernel technology: Click-os","type": "configuration","time":"","color" :""}

                    Clients.send(json.dumps(tcpLOG)+"\n")


                    print remote_testbeds

                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Resource Orchestrator","message":"Sending complete slice configuration to the Service Orchestrator: Slice is operating, congratulations.","type":"communication","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    tcpMessage={"key":"necosTCP","source":source,"target":"nodeWAN","config":json_object}
                    Clients.send(json.dumps(tcpMessage)+"\n")
                else:
                    print("")
            else:
                print("")


    except (SystemExit, KeyboardInterrupt):
        print ("Exiting...")
        break

print ("Closing socket")
Clients.close
