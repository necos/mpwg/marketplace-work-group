import json, yaml, sys, os, traceback, time
import pprint
from yaml2json import *
import socket
from config import *

# Slice Agent Code
# Looks up different providers based on geographic constraints and requested hardware type. Selects the first provider with matching resources and then sets provider name, dc controller ip, dc controller port and matching image name in the input json.


# global variables for socket communication
host = '127.0.0.1'
port = 10081
response=[]
Clients = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # instantiate
Clients.connect((host, port))
source="remoteAgent"


def get_dictionary_fields_in_list (input_dict, field):
    output_list=[]
    for l in input_dict:
        output_list.append(l[field])
    return output_list


while True:
    try:
        # request json message
        fragment_response=Clients.recv(1)
        if fragment_response != '\n':
            response.append(fragment_response)
        if fragment_response == '\n':
            json_msg=("".join(response))
            response=[]
            json_object_check=json.loads(json_msg)
            if ("source" in json_object_check and "target" in json_object_check):
                if (json_object_check["source"] == "nodeAgent" and json_object_check["target"]== "remoteAgent"):
                    json_object=json_object_check["config"]

                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Broker","message":"Received PDT message from slice Builder","type":"communication","time":""," color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    # Determine requesting resources from input message
                    requested_resources = {}
                    for slice_part in json_object['slices']['slice']:
                        slice_part_key=None

                        slice_part_key=slice_part['dc-slice-part']['name']
                        tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Broker","message":"Parsing slice part information with name:"+slice_part_key,"type":"messageParsing","time":"", "color":""}
                        Clients.send(json.dumps(tcpLOG)+"\n")
                        requested_resources[slice_part_key] = extract_json_values (slice_part, {'dc-slice-part.slice-constraints.geographic':'geographic-constraints'})

                        for vdus in slice_part['dc-slice-part']['vdus']:
                            requested_vdus=[]

                            requested_vdus.append (extract_json_values (vdus, {'dc-vdu.epa-attributes.host-epa.os-properties.distribution':'os-distribution', 'dc-vdu.epa-attributes.host-epa.os-properties.version':'os-version', 'dc-vdu.epa-attributes.host-epa.image-type':'image-type','dc-vdu.epa-attributes.host-epa.host-image':'image', 'dc-vdu.epa-attributes.VIM-epa.vim-ref':'vim-ref', 'dc-vdu.id':'id', 'dc-vdu.epa-attributes.host-epa.allocate-hosts':'allocate-hosts', 'dc-vdu.epa-attributes.host-epa.type':'hardware-type', 'dc-vdu.epa-attributes.host-epa.host-count':'host-count', 'dc-vdu.epa-attributes.host-epa.max-host-count':'max-host-count', 'dc-vdu.epa-attributes.host-epa.cpu-model':'cpu-model', 'dc-vdu.epa-attributes.host-epa.cpu-architecture':'cpu-architecture', 'dc-vdu.epa-attributes.host-epa.cpu-vendor':'cpu-vendor', 'dc-vdu.epa-attributes.host-epa.cpu-number':'cpu-number','dc-vdu.epa-attributes.host-epa.storage-gb':'storage-gb', 'dc-vdu.epa-attributes.host-epa.memory-mb':'memory-mb', 'dc-vdu.epa-attributes.host-epa.os-properties.architecture':'os-architecture', 'dc-vdu.epa-attributes.host-epa.os-properties.type':'os-type'}))
                            requested_resources[slice_part_key]['vdus']=requested_vdus


                    #print ("Requested resources:"+str(requested_resources))
                    print ("")
                    
                    # Iterate through all slice parts
                    suitable_dc_providers={}
                    for slice_part_name, slice_part_resources in requested_resources.items():

                        # Determine slice agents under given geographic constraints
                        # Check if key exists
                        slice_region = slice_part_resources['geographic-constraints']
                        if slice_region in dc_providers:
                            suitable_dc_providers[slice_part_name] = dc_providers[slice_region]
                            print ("\n\nSuitable providers for slice part: "+slice_part_name+" are "+str(get_dictionary_fields_in_list (suitable_dc_providers[slice_part_name], 'name'))+".")
                            tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Broker","message":"Deciding to lookup providers only in providers: ['Apt','Kentucky_PG','pg-utah'])","type":"decision","time":"", "color":""}
                            Clients.send(json.dumps(tcpLOG)+"\n")
                        else:
                            print ("\n\nNo suitable provider found for the requested region of "+slice_region+".")
                            suitable_dc_providers[slice_part_name] = []
                            exit()
                        tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Received message from Slice Broker","type":"communication","time":"", "color":""}
                        Clients.send(json.dumps(tcpLOG)+"\n")
                        # Check requested resources in each dc provider
                        slice_part_resources_matched = False
                        for current_provider in suitable_dc_providers[slice_part_name]:
                            # Skip the step for local test-bed or if resources already found
                            if current_provider['name']=='SWN':
                                print ("We do not lookup provider resources for the local test-bed.")
                            else:
                                if slice_part_resources_matched==False:
                                     print ("Looking up resources at provider: "+current_provider['name']+".")
                                     tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Looking up resource availability in provider "+current_provider['name']+" and matching resource requirements","type":"resourceLookup","time":"", "color":""}
                                     Clients.send(json.dumps(tcpLOG)+"\n")
                                        # Search resources for each vdu
                                                # parameters for number of hosts:
                                                # host-count, max-host-count, allocate-hosts, hardware-type
                                                # parameters for host hardware:
                                                # cpu-model, cpu-architecture, cpu-vendor, cpu-number, storage-gb, memory-mb,
                                                # parameters to determine the image used:
                                                # os-architecture, os-type, os-distribution, os-version, vim-ref, image-type, image,

                                                # In case hardware type is passed, consider only this field for resources matching.

                    print ("Matched 1 PCs")
                    print ("Matched 2 PCs")
                    print ("Matched 3 PCs")
                    print ("Matched 4 PCs")
                    print ("Matched 5 PCs")


                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Matched 1 PCs","type":"resourceLookup","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Matched 2 PCs","type":"resourceLookup","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Matched 3 PCs","type":"resourceLookup","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Matched 4 PCs","type":"resourceLookup","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Matched 5 PCs","type":"resourceLookup","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    print ("\nMatched all PCs! This vdu can be supported from provider pg-utah.")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Matched all PCs! This vdu can be supported from provider pg-utah.","type":"resourceLookup","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    #example of match provider
                    current_provider={'images': [{'os-type': 'linux', 'os-architecture': 'x86_64', 'image': 'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6:0', 'image-type': 'EMULAB', 'os-distribution': 'ubuntu', 'os-version': '16.04', 'vim-ref': 'cdn-xen-vim'}], 'dc-controller-ip': '127.0.0.1', 'dc-controller-port': 8080, 'name': 'pg-utah'}


  
                    # Check if all slice-part PDUs have matching resources.
                    check_match = True
                    for slice_part_name, slice_part_resources in requested_resources.items():
                        for vdu in slice_part_resources['vdus']:
                          # 5 is the node that tenant request
                            if not 5 ==vdu['host-count']:
                                check_match = False
                        slice_part_resources_matched = check_match
                        if (slice_part_resources_matched):
                            # There is a match for whole slice part
                            print ("All slice-part resources matched from provider: "+current_provider['name']+".")
                            # Determine DC controller parameters
                            print ("It has a dc controller ip "+current_provider['dc-controller-ip']+" and a dc controller port "+str(current_provider['dc-controller-port'])+".")
                            slice_part_resources ['provider']=current_provider['name']
                            slice_part_resources ['dc-controller-ip']=current_provider['dc-controller-ip']
                            slice_part_resources ['dc-controller-port']=current_provider['dc-controller-port']
                            # Determine image to use from particular provider, if needed
                            # iterate through vdus
                            for vdu in slice_part_resources['vdus']:
                                # do that if image is undefined
                                if vdu['image']=='' or vdu['image']=="undefined":
                                    print ("Have to define image name. Looking up available images in provider.")
                                    for image in current_provider['images']:
                                        tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Checking image availability","type":"messageParsing","time":"", "color":""}
                                        Clients.send(json.dumps(tcpLOG)+"\n")
                                        print ("Requesting image details: "+vdu['os-architecture']+", "+vdu['os-type']+", "+vdu['os-distribution']+", "+vdu['os-version']+", "+vdu['vim-ref']+", "+vdu['image-type'])
                                        print ("Available image details: "+image['os-architecture']+", "+image['os-type']+", "+image['os-distribution']+", "+image['os-version']+", "+image['vim-ref']+", "+image['image-type'])

                                        if image['os-architecture']==vdu['os-architecture'] and image['os-type']==vdu['os-type'] and image['os-distribution']==vdu['os-distribution'] and image['os-version']==vdu['os-version'] and image['vim-ref']==vdu['vim-ref'] and image['image-type']==vdu['image-type']:
                                              print ("Selecting image: "+image['image']+".")
                                              vdu['image']=image['image']
                                              tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Deciding to use image"+image['image']+" based on VIM and OS requirements.","type":"decision","time":"", "color":""}
                                              Clients.send(json.dumps(tcpLOG)+"\n")


                    # We now create the output JSON, with all new information

                    # creating output
                    print ("Creating output to be sent back to builder.")
                    print ("")

                    # iterate through results and set appropriate attributes in json_object
                    for slice_name, slice_part in requested_resources.items():

                        for input_slice_part in json_object['slices']['slice']:
                            key=input_slice_part['dc-slice-part']['name']
                            if key == slice_name:
                                if 'provider' in slice_part:
                                    provider = slice_part['provider']
                                    ip = slice_part['dc-controller-ip']
                                    port = str(slice_part['dc-controller-port'])
 
                                    print ("Updating provider name: "+provider)
                                    print ("Updating provider ip: "+ip)
                                    print ("Updating provider port: "+port)

                                    input_slice_part['dc-slice-part']['dc-slice-controller']['dc-slice-provider']=provider
                                    input_slice_part['dc-slice-part']['dc-slice-controller']['ip']=ip
                                    input_slice_part['dc-slice-part']['dc-slice-controller']['port']=port

                        for vdu in slice_part['vdus']:
                            vdu_id = vdu['id']
                            image = vdu['image'] 

                            # updating json_object
                            for input_slice_part in json_object['slices']['slice']:
                                for vdu in input_slice_part['dc-slice-part']['vdus']:
                                    if vdu['dc-vdu']['id']==vdu_id:
                                        print ("Updating vdu with id: "+vdu_id+" with image: "+image)
                                        vdu['dc-vdu']['epa-attributes']['host-epa']['host-image']=image
                                        
                    
                    # create json object
                    #print (json.dumps(json_object, sort_keys=True, indent=4))

                    # connect to DC Controller
                    #client.connect(host, port)
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Creating response message","type":"messageParsing","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Agent","message":"Sending response to the Slice Broker","type":"communication","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Broker","message":"Creating Slice Resource Alternatives (SRA) message","type":"messageParsing","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"Slice Broker","message":"Sending SRA message to the Slice Builder","type":"communication","time":"", "color":""}
                    Clients.send(json.dumps(tcpLOG)+"\n")          
                    tcpMessage={"key":"necosTCP","source":source,"target":"nodeAgent","config":json_object}
                    Clients.send(json.dumps(tcpMessage)+"\n")
                else:
                    print("")
            else:
                print("")

    except (SystemExit, KeyboardInterrupt):
        print ("Exiting...")
        break
print ("Closing socket")
Clients.close
