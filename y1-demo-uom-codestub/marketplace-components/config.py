#General parameters
code_path = ''

# Authentication parameters
username=''
local_testbed_username=''
project='uom-necos'
cert_path='/root/.ssh/'
key_path='/root/.ssh/'

# DC Slice Controler Parameters
# Image related 
default_gre_image = 'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6'
xen_image = 'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6'
default_image = 'urn:publicid:IDN+clemson.cloudlab.us+image+schedock-PG0:docker-ubuntu16'

# Slice Agent Parameters
# Defines a list with accessible testbeds and example configuration that includes dc controller configuration parameters and available images
dc_providers={'GREECE': [{'name':'SWN','dc-controller-ip':'127.0.0.1','dc-controller-port':10081, 'images':[{'image':'Ubuntu-16.04-STD', 'image-type':'EMULAB', 'os-architecture':'x86_64', 'os-type':'linux', 'os-distribution':'ubuntu', 'os-version':'16.04', 'vim-ref':''}]}], 'EUROPE':[{'name':'Wall1_PG','dc-controller-ip':'127.0.0.1','dc-controller-port':8080, 'images':''}, {'name':'Wall2_PG','dc-controller-ip':'127.0.0.1','dc-controller-port':10081, 'images':''}, {'name':'wilab_PG','dc-controller-ip':'127.0.0.1','dc-controller-port':10081, 'images':''}], 'USA':[{'name':'Apt','dc-controller-ip':'127.0.0.1','dc-controller-port':10081, 'images':'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6:0'}, {'name':'Kentucky_PG','dc-controller-ip':'127.0.0.1','dc-controller-port':8080, 'images':'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6:0'},{'name':'pg-utah','dc-controller-ip':'127.0.0.1','dc-controller-port':10081, 'images': [{'image':'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6:0', 'image-type':'EMULAB', 'os-architecture':'x86_64', 'os-type':'linux', 'os-distribution':'ubuntu', 'os-version':'16.04', 'vim-ref':'cdn-xen-vim'}]}]}

# WAN Controller Parameters
deploy_tunnels = True
tunneling_option = "ONE-TO-MANY"
main_tunneling_endpoint = "swn"
connect_to_local_testbed = True

# WAN Controller Parameters for communicating with the Service Orchestrator
orchestratorIP= ''
orchestratoruser= ''
TcpPortOrchestrator = ''
