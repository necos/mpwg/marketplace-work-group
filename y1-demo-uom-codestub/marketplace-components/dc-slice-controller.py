import json
from yaml2json import *
import os
import random
import socket
from config import *


# general DC Slice Controller parameters
remote_testbeds=[]
local_testbed={}
#connect_to_local_testbed = False
deploy_routers = True
slice_duration = 240

# slice output parameters
hostIPs={}
privateHostIPs={}
privateRouterIPs={}
routerIPs={}
routerHostName={}
hostNames={}


# global variables for socket communication
host = '127.0.0.1'
port = 10082
Clients = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
Clients.connect((host, port))
response=[]
source="remoteDC"

global local_sub
local_sub="10.1.0.1"
first_prefix_subnet, second_prefix_subnet, third_prefix_subnet, four_prefix_subnet = local_sub.split('.')
counter=int(second_prefix_subnet)


requests = {}
def determine_image_to_deploy (vim_type, vim_version):
    if vim_type=='XEN' and vim_version=='4.6':
        return xen_image
    else:
        return default_image

def check_if_value_is_defined (value):
    if value=='':
        return False
    else:
        return True

def increment():
    global counter
    counter =counter+1

def check_if_Local_subnet (k,value):
    if value==''or value== "undefined":
        first_prefix_subnet, second_prefix_subnet, third_prefix_subnet, four_prefix_subnet = k.split('.')
        k=first_prefix_subnet+"."+str((int(second_prefix_subnet)+counter))+"."+third_prefix_subnet
        increment()
        return k

def check_if_Virtual_subnet (k,value):
    if value=='' or value== "undefined":
        first_prefix_subnet, second_prefix_subnet, third_prefix_subnet, four_prefix_subnet = k.split('.')
        k=first_prefix_subnet+"."+str((int(second_prefix_subnet)+counter))
        increment()
        return k

def create_dc_controller_input (json_message):
    json_object = json.loads(json_message)

    # iterate through DC Slice Parts
    sp=[];
    sp2=[];
    for i,slice_part in enumerate(json_object['slices']['slice']):

        sp.append(extract_json_values(slice_part['dc-slice-part'], {'name':'SlicePartName','dc-slice-controller.dc-slice-provider':'Provider','slice-part-timeframe.service-duration':'Duration','vim.hypervisor.type':'VIMType','vim.hypervisor.version':'VIMVersion','dc-slice-point-of-presence.host-ip':'RouterIP','dc-slice-point-of-presence.requirements.bandwidth':'TunnelBandwidth','dc-slice-point-of-presence.router-type':'TunnelType'}))
        for j,vdus in enumerate(slice_part['dc-slice-part']['vdus']):

            sp2.append(extract_json_values(vdus,{'dc-vdu.epa-attributes.host-epa.host-ips':'NodeIPs','dc-vdu.epa-attributes.host-epa.type':'NodeType','dc-vdu.epa-attributes.host-epa.host-count':'NodesNumber','dc-vdu.epa-attributes.host-epa.subnet':'LocalPhysicalNetworkIPPrefix','dc-vdu.virtual-nodes-subnet':'VirtualNetworkIPPrefix','dc-vdu.epa-attributes.host-epa.allocate-hosts':'AllocateHosts'}))

            # decide image to deploy

            if (sp[i]['Provider']=='swn'):
                # configure local_testbed
                # example values:
                # local_testbed = {'Name':'swn', 'RouterIP':'195.251.209.210', 'NodeIPs':[], 'UserName':'xvalsama', 'LocalPhysicalNetworkIPPrefix':'10.1.0', 'VirtualNetworkIPPrefix':'', 'ConfigureHypervisor':False}
                tcpLOG ={"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Parsing slice part information with name: dc-slice-part1","type":"messageParsing","time":"","color" :""}
                Clients.send(json.dumps(tcpLOG)+"\n")
                local_testbed.update({'Provider':'swn', 'RouterIP':sp[i]['RouterIP'], 'NodeIPs':sp2[i]['NodeIPs'], 'Username':local_testbed_username, 'LocalPhysicalNetworkIPPrefix':sp2[i]['LocalPhysicalNetworkIPPrefix'], 'VirtualNetworkIPPrefix':sp2[i]['VirtualNetworkIPPrefix'], 'ConfigureHypervisor':check_if_value_is_defined (sp[i]['VIMType']), 'Deploy':sp2[i]['AllocateHosts']})


            else:
               # deploy federated testbed
               # example values:
               # remote_testbeds = [{'Name':'pg-utah', 'NodeType':'pc3000', 'NodesNumber':2, 'Image':'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v5:0', 'GRERouterImage':'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v5', 'LocalPhysicalNetworkIPPrefix':'10.2.0', 'VirtualNetworkIPPrefix':'10.3','Deploy':True, 'DeployRouter':True, 'ConfigureHypervisor':True}]

                # decide image to deploy
                image = determine_image_to_deploy (sp[i]['VIMType'], sp[i]['VIMVersion'])
                tcpLOG ={"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Parsing slice part information with name: dc-slice-part2","type":"messageParsing","time":"","color" :""}
                Clients.send(json.dumps(tcpLOG)+"\n")
                current_testbed={'Provider':sp[i]['Provider'], 'NodeType':sp2[i]['NodeType'], 'NodesNumber':sp2[i]['NodesNumber'], 'Image': image, 'GRERouterImage': default_gre_image, 'RouterIP': sp[i]['RouterIP'], 'NodeIPs': sp2[i]['NodeIPs'], 'Username': username, 'LocalPhysicalNetworkIPPrefix': check_if_Local_subnet (local_sub,sp2[i]['LocalPhysicalNetworkIPPrefix']), 'VirtualNetworkIPPrefix':check_if_Virtual_subnet (local_sub,sp2[i]['VirtualNetworkIPPrefix']), 'ConfigureHypervisor':check_if_value_is_defined (sp[i]['VIMType']), 'Deploy':sp2[i]['AllocateHosts'], 'DeployRouter':check_if_value_is_defined(sp[i]['TunnelType'])}

                remote_testbeds.append(current_testbed)

#                print remote_testbeds
                if check_if_value_is_defined(sp[i]['Duration']):
                    slice_duration = sp[i]['Duration']

                deploy_routers = current_testbed['DeployRouter']


def slice_creation_output (json_object):

    print ("Creating output to be sent back to wan.")
    print ("")

    print ("")
    print ("The IPs of the allocated machines follow:")

    print ("")
    print ("The router IPs is:")
    routerIPs={u'pg-utah': '155.98.36.75'}
    print (routerIPs)
    print("The hosts IPs are:")
    hostIPs={u'pg-utah': ['155.98.36.68', '155.98.36.65', '155.98.36.77', '155.98.36.67', '155.98.36.71']}
    print hostIPs
    # Keep host and router IPs in remote_testbeds data structure

    # Store host public and private IPs
    print ("The final result follows.")
    remote_testbeds=[{'GRERouterImage': 'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6', 'NodeIPs': ['155.98.36.68', '155.98.36.65', '155.98.36.77', '155.98.36.67', '155.98.36.71'], 'LocalPhysicalNetworkIPPrefix': '10.2.0', 'NodesNumber': 5, 'ConfigureHypervisor': True, 'Provider': u'pg-utah', 'RouterIP': '155.98.36.75', 'PrivateRouterIP': '10.2.0.1', 'VirtualNetworkIPPrefix': '10.3', 'PrivateNodeIPs': ['10.2.0.2', '10.2.0.3', '10.2.0.4', '10.2.0.5', '10.2.0.6'], 'Username': 'XronisV', 'NodeType': u'd430', 'Deploy': True, 'Image': 'urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6', 'RouterHostName': 'pc775.emulab.net', 'NodeHostNames': ['pc768.emulab.net', 'pc765.emulab.net', 'pc777.emulab.net', 'pc767.emulab.net', 'pc771.emulab.net'], 'DeployRouter': True}]
    print (remote_testbeds)





    for j,t in enumerate(remote_testbeds):

        for i,input_slices in enumerate(json_object['slices']['slice']):
            if json_object['slices']['slice'][i]['dc-slice-part']['dc-slice-controller']['dc-slice-provider'] == remote_testbeds[j]['Provider']:
                for input_dc_slice_part in input_slices['dc-slice-part']['dc-slice-point-of-presence']:

                    RouterIP = remote_testbeds[j]['RouterIP']
                    RouterHostName = remote_testbeds[j]['RouterHostName']
                    PrivateRouteIP = remote_testbeds[j]['PrivateRouterIP']
                    if not input_slices['dc-slice-part']['dc-slice-point-of-presence']['host-ip'] or input_slices['dc-slice-part']['dc-slice-point-of-presence']['host-ip'] == "undefined":
                        input_slices['dc-slice-part']['dc-slice-point-of-presence']['host-ip']=RouterIP
                        input_slices['dc-slice-part']['dc-slice-point-of-presence']['host-name']=RouterHostName
                        input_slices['dc-slice-part']['dc-slice-point-of-presence']['private-host-ip']=PrivateRouteIP

                for vdus in input_slices['dc-slice-part']['vdus']:

                    host_ips = remote_testbeds[j]['NodeIPs']
                    host_private_ips = remote_testbeds[j]['PrivateNodeIPs']
                    host_names = remote_testbeds[j]['NodeHostNames']
                    subnet=remote_testbeds[j]['LocalPhysicalNetworkIPPrefix']
                    subnet_virtual=remote_testbeds[j]['VirtualNetworkIPPrefix']
    
                    for vdu in vdus['dc-vdu']:

                        if not vdus['dc-vdu']['epa-attributes']['host-epa']['host-ips'] or vdus['dc-vdu']['epa-attributes']['host-epa']['host-ips'] == "undefined":
                            vdus['dc-vdu']['epa-attributes']['host-epa']['host-ips']=host_ips
                            vdus['dc-vdu']['epa-attributes']['host-epa']['host-private-ips']=host_private_ips
                            vdus['dc-vdu']['epa-attributes']['host-epa']['host-names']=host_names
                            vdus['dc-vdu']['epa-attributes']['host-epa']['subnet']= subnet
                            vdus['dc-vdu']['virtual-nodes-subnet']=subnet_virtual

    tcpLOG ={"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Creating response to Slice-builder","type":"messageParsing","time":"","color" :""}
    Clients.send(json.dumps(tcpLOG)+"\n")
    #print (json.dumps(json_object, sort_keys=True, indent=4))
    return json_object



# Main DC Slice Controller code

# Define socket daemon
while True:
    try:
        fragment_response=Clients.recv(1)
        if fragment_response != '\n':
            response.append(fragment_response)
        if fragment_response == '\n':
            json_msg=("".join(response))
            response=[]
            json_object_check=json.loads(json_msg)
            if ("source" in json_object_check and "target" in json_object_check):
                if (json_object_check["source"] == "nodeDC" and json_object_check["target"]== "remoteDC"):
                    json_object=json.dumps(json_object_check["config"])
                    raw_json_object=json_object_check["config"]                    

                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Received message from slice Builder","type":"communication","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    create_dc_controller_input(json_object)
                    print ("remote_testbes is: ")
                    print remote_testbeds
                   
                    # create slice request
                    tcpLOG ={"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Configuring dc-slice-part's internal network","type":"configuration","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")

                    # deploy slice
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Deploying 5 physical machines with image: urn:publicid:IDN+emulab.net+image+unic-PG0:XEN4-6v6","type":"deployment","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")


                    # produce slice creation output
                    json_output_object = slice_creation_output(raw_json_object)

                    # connect to DC Controller
                    tcpLOG = {"key":"necosTCP","source":source,"target":"nodeLog","sourceTitle":"DC-Slice-Controller","message":"Sending response to the Slice Builder","type":"communication","time":"","color" :""}
                    Clients.send(json.dumps(tcpLOG)+"\n")
                    tcpMessage={"key":"necosTCP","source":"remoteDC","target":"nodeDC","config":json_output_object}
                    Clients.send(json.dumps(tcpMessage)+"\n")

                else:
                    print("")
            else:
                print("")

        # send back reponse
        #server.send({"response":data})
    except (SystemExit, KeyboardInterrupt):
        print ("Exiting...")
        break

print ("Closing socket")
Clients.close
