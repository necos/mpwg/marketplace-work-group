## NECOS source code from UOM

This source code is based on the y1 demo from UOM. We cleaned up up the code (i.e., removed test-bed specific code) and created an emulation of all the involved interactions in this demo. The YAML/JSON messages exchanged are currently associated with the particular service specifications/slice definitions (i.e., the touristic content delivery service).

The source code can be either downloaded through the git or executed based on Docker containers. It consists of two parts:
* The main NECOS components up-to and not including the Slice Broker, implemented as NodeJS [Node-RED](https://nodered.org) nodes, i.e., sourcecode folder [e2e-slice-components](https://github.com/SWNRG/necos/tree/master/code-stub/e2e-slice-components). These are relevant to the [end-to-end slice WG](http://clayfour.ee.ucl.ac.uk/dokuwiki/doku.php?id=necos:workpackages_activities_and_deliverables:wp5:e2eswg:e2eswg).
* The Marketplace-related components implemented in Python. The sourcecode folder name is [marketplace-components](https://github.com/SWNRG/necos/tree/master/code-stub/marketplace-components). These components are relevant to the [Marketplace WG](http://clayfour.ee.ucl.ac.uk/dokuwiki/doku.php?id=necos:workpackages_activities_and_deliverables:wp5:mpwg:mpwg).

The latter components may act as a starting point for the Marketplace WG, but also as an emulation of the marketplace components for a complete operation of the slice deployment process.

### Instructions on how to execute the code through Docker containers
The complete e2e slice deployment operation, including the marketplace interactions, requires two different containers (i.e., one for the e2e slice and one for the marketplace components, respectively). 

#### Container for the e2e slice components
The first container includes toy implementations of the main NECOS architectural components. They have been developed in Javascript/NodeJS through a micro-services aproach. Practically, we defined a slice deployment workflow through the Node-RED tool. The same container includes a centralized debug window, visualizing all the exchanged and debug messages from the involved NECOS components.

To download the container locally (optional step):

`docker pull swnuom/necos-e2e-slice`

To execute the container:

`docker run -it -p 1880:1880 -p 10081:10081 -p 10082:10082 -p 10083:10083 swnuom/necos-e2e-slice`

The container requires a number of ports open, i.e., the ports 1880 (i.e., to access the GUI) and 10081, 10082, 10083 (i.e., for the communication with the marketplace components). Alternatively, someone may use the `--network="host"`, for an execution in the same physical host.

Browse http://{host-ip}:1880 to access the Node-RED workflow (e.g., http://127.0.0.1:1880).

The URL to access to the Node-RED Dashboard (i.e., the NECOS centralized debug window) is: http://127.0.0.1:1880/ui/#/0

#### Container for the Marketplace components
The second container supports three basic marketplace components (i.e., the Broker/Agents, the DC Slice Controller, and the WAN Slice Controller). To download the container (optional step): 

`docker pull swnuom/necos-marketplace`

To execute each component, you can have different container instances based on the same image. A short analysis of each component and the equivalanet execution instructions follow.

*Slice Broker/Agent*: 
At this point, the Slice Broker is embedded with the Slice Agents, for simplicity (they should be decoupled). We plan to emulate different real providers in Europe and Brazil, based on real XML outputs from federated testbeds (i.e., rspec-based listresources command outputs and manifests after host allocations). To execute a container with a Slice Broker/Agent running:

`docker run -it --network="host" swnuom/necos-marketplace python /root/y1_demo_Necos/broker_agent.py`

*DC Slice Controller*: 
This is code stub for different DC Slice Controller implementations. To execute the DC Slice Controller example:

`docker run -it --network="host" swnuom/necos-marketplace python /root/y1_demo_Necos/dc-slice-controller.py`

*WAN Slice Controller*: 
This is a code stub for different WAN Slice Controller implementations. At this point, we assume a GRE tunnel establishment. To execute this component:

`docker run -it --network="host" swnuom/necos-marketplace python /root/y1_demo_Necos/wan-slice-controller.py`
