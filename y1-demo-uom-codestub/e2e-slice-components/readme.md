## Basic documentation of the e2e slice components

Due to the complex installation process of Node-RED, involving addition of extra code in the Node-RED itself, the best process is to extract the source code from the equivalent container, i.e., extract complete Node-RED folders.

The file `slice_deployment_input.yaml` represents the slice requirements, defined through the Slice Activator. The file is aligned to the NECOS information model. This example is based on the Touristic use-case.
