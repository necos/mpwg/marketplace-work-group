# Marketplace work group

This repository contains source code implemented from the [Markeplace working group](http://clayfour.ee.ucl.ac.uk/dokuwiki/doku.php?id=necos:workpackages_activities_and_deliverables:wp5:mpwg:mpwg) of NECOS project.

Source-code available:
* The [y1-demo-uom-codestub](https://gitlab.com/necos/marketplace-work-group/tree/master/y1-demo-uom-codestub) is an emulation of the first year demo implemented by UOM. It may serve as a basis for the implementation of many NECOS components, including the Marketplace-related ones.